import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from 'pages/Home';
import ProductPage from 'pages/ProductPage';
import SignUp from 'pages/SignUp';
import SignIn from 'pages/SignIn';

class Routes extends Component {
  getProductPage = ({ match }) => {
    return <ProductPage id={match.params.id} />;
  };
  render() {
    return (
      <Switch>
        <Route path="/SignIn" exact component={SignIn} />
        <Route path="/signUp" exact component={SignUp} />
        <Route path="/product/:id" exact children={this.getProductPage} />
        <Route path="/" component={Home} />
      </Switch>
    );
  }
}
export default Routes;
