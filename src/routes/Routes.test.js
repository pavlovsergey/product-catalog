import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Routes from '.';

describe('Snapshot for component Routes', () => {
  it('should render correctly', () => {
    const component = shallow(<Routes />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
