export const getObjectFromArray = arr => {
  return arr.reduce((acc, item) => {
    const { id } = item;
    acc[id] = item;
    return acc;
  }, {});
};
