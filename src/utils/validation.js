import passwordValidator from 'password-validator';

export const validationUserName = (userName, errors) => {
  if (!userName) {
    errors.isError = true;
    errors.userName = ` userName is required,`;
  }

  if (userName.length < 5) {
    errors.isError = true;
    errors.userName += ` minimum length 8`;
  }
  return errors;
};

export const validationPasword = (password, errors) => {
  const keyPasswordError = {
    min: ' minimum length 8;',
    max: ' maximum length 100;',
    oneOf: ' be not primitive;'
  };

  const schemaPasswordValidator = new passwordValidator();
  schemaPasswordValidator
    .is()
    .min(8)
    .is()
    .max(100)
    .not()
    .oneOf(['Password', 'Passw0rd', 'Password123', 'password']); // Blacklist these values};

  if (schemaPasswordValidator.validate(password, { list: true }).length > 0) {
    errors.isError = true;
    errors.password = 'Password must have:';
    schemaPasswordValidator
      .validate(password, { list: true })
      .forEach(error => {
        errors.password += keyPasswordError[error] || '';
      });
  }
  return errors;
};

export const validationRepeatPassword = (password, repeatPassword, errors) => {
  if (password !== repeatPassword) {
    errors.isError = true;
    errors.repeatPassword =
      'The password and confirm password fields do not match.';
  }
  return errors;
};
