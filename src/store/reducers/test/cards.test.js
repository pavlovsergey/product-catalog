import cards from '../cards';
import { LOAD_CARDS } from 'store/constants/cards';

describe('cards reducer', () => {
  it('should return the initial state', () => {
    expect(cards(undefined, {})).toEqual({});
  });

  it('should handle LOAD_CARDS', () => {
    const id = 1;
    expect(
      cards(
        {},
        {
          type: LOAD_CARDS,
          payload: { data: [{ id }] }
        }
      )
    ).toEqual({
      [id]: { id }
    });
  });
});
