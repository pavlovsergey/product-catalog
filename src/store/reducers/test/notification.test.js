import notification from '../notification';
import {
  CREATE_NOTIFICATION,
  CLEAR_NOTIFICATION
} from 'store/constants/notification';

describe('notification reducer', () => {
  it('should return the initial state', () => {
    expect(notification(undefined, {})).toEqual({});
  });

  it('should handle  CREATE_NOTIFICATION', () => {
    const id = 1,
      message = 'message';
    const expectedReducer = { [id]: { id, message } };
    expect(
      notification(
        {},
        {
          type: CREATE_NOTIFICATION,
          payload: { id, message }
        }
      )
    ).toEqual({ ...expectedReducer });
  });

  it('should handle CLEAR_NOTIFICATION', () => {
    const id = 1;
    expect(
      notification({ [id]: id }, { type: CLEAR_NOTIFICATION, payload: { id } })
    ).toEqual({});
  });
});
