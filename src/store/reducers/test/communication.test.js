import communication from '../communication';
import {
  LOADING_CARDS,
  LOAD_CARDS_FAIL,
  LOADING_REVIEWS,
  LOAD_REVIEWS_FAIL
} from 'store/constants/communication';
import { LOAD_CARDS } from 'store/constants/cards';
import { LOAD_REVIEWS } from 'store/constants/reviews';

describe('cards reducer', () => {
  it('should handle LOADING_CARDS', () => {
    let expectObject = { cards: { isLoading: true } };
    expect(communication({}, { type: LOADING_CARDS })).toEqual(expectObject);
  });

  it('should handle LOAD_CARDS', () => {
    const id = 1;
    const data = [{ id }];
    let expectObject = {
      cards: { isLoad: true, isLoading: false },
      reviews: {
        [id]: {
          error: {
            message: '',
            name: ''
          },
          isErrorLoad: false,
          isLoad: false,
          isLoading: false
        }
      }
    };
    expect(communication({}, { type: LOAD_CARDS, payload: { data } })).toEqual(
      expectObject
    );
  });

  it('should handle LOAD_CARDS_FAIL ', () => {
    const message = 'message';
    let expectObject = {
      cards: {
        isLoading: false,
        isErrorLoad: true,
        error: { message }
      }
    };
    expect(
      communication({}, { type: LOAD_CARDS_FAIL, payload: { message } })
    ).toEqual(expectObject);
  });

  it('should handle LOADING_REVIEWS', () => {
    const id = 1;
    let expectObject = { reviews: { [id]: { isLoading: true } } };
    expect(
      communication({ reviews: {} }, { type: LOADING_REVIEWS, payload: { id } })
    ).toEqual(expectObject);
  });

  it('should handle LOAD_REVIEWS', () => {
    const id = 1;
    let expectObject = {
      reviews: { [id]: { isLoad: true, isLoading: false } }
    };
    expect(
      communication({ reviews: {} }, { type: LOAD_REVIEWS, payload: { id } })
    ).toEqual(expectObject);
  });

  it('should handle LOAD_REVIEWS_FAIL', () => {
    const id = 1;
    const message = 'message';
    let expectObject = {
      reviews: {
        [id]: {
          isLoading: false,
          isErrorLoad: true,
          error: { message }
        }
      }
    };
    expect(
      communication(
        { reviews: {} },
        { type: LOAD_REVIEWS_FAIL, payload: { message, id } }
      )
    ).toEqual(expectObject);
  });
});
