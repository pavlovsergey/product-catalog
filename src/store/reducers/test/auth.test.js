import auth, { stateAuthDefault } from '../auth';
import {
  CREATE_USER,
  CREATE_USER_FAIL,
  LOGIN_USER,
  LOGIN_USER_FAIL,
  LOGOUT_USER
} from 'store/constants/auth';

describe('auth reducer', () => {
  it('should return the initial state', () => {
    expect(auth(undefined, {})).toEqual(stateAuthDefault);
  });

  it('should handle CREATE_USER', () => {
    const data = {
      role: 'role',
      username: 'username',
      errCreateUser: ''
    };
    expect(
      auth(
        {},
        {
          type: CREATE_USER,
          payload: data
        }
      )
    ).toEqual(data);
  });

  it('should handle CREATE_USER_FAIL', () => {
    const message = 'message';

    expect(
      auth(
        {},
        {
          type: CREATE_USER_FAIL,
          payload: { message }
        }
      )
    ).toEqual({ errCreateUser: message });
  });

  it('should handle LOGIN_USER', () => {
    const data = {
      role: 'role',
      username: 'username',
      errCreateUser: ''
    };
    expect(
      auth(
        {},
        {
          type: LOGIN_USER,
          payload: data
        }
      )
    ).toEqual(data);
  });

  it('should handle LOGIN_USER_FAIL', () => {
    const message = 'message';

    expect(
      auth(
        {},
        {
          type: LOGIN_USER_FAIL,
          payload: { message }
        }
      )
    ).toEqual({ errLogInUser: message });
  });

  it('should handle LOGOUT_USER', () => {
    expect(auth({}, { type: LOGOUT_USER })).toEqual(stateAuthDefault);
  });
});
