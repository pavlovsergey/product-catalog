import reviews from '../reviews';
import { LOAD_REVIEWS, CREATE_REVIEW } from 'store/constants/reviews';

describe('reviews reducer', () => {
  it('should return the initial state', () => {
    expect(reviews(undefined, {})).toEqual({});
  });

  it('should handle LOAD_REVIEWS', () => {
    const id = 1;
    expect(
      reviews(
        {},
        {
          type: LOAD_REVIEWS,
          payload: { data: [{ id }], id }
        }
      )
    ).toEqual({
      [id]: [{ id }]
    });
  });

  it('should handle CREATE_REVIEW with empty store', () => {
    expect(reviews({}, { type: CREATE_REVIEW })).toEqual({});
  });

  it("should handle CREATE_REVIEW with don't empty store", () => {
    const id = 1;
    const expectedReducer = { [id]: [{ product: id }] };
    expect(
      reviews({ [id]: [] }, { type: CREATE_REVIEW, payload: { product: id } })
    ).toEqual(expectedReducer);
  });
});
