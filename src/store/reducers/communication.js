import {
  LOADING_CARDS,
  LOAD_CARDS_FAIL,
  LOADING_REVIEWS,
  LOAD_REVIEWS_FAIL
} from 'store/constants/communication';
import { LOAD_CARDS } from 'store/constants/cards';
import { LOAD_REVIEWS } from 'store/constants/reviews';
import {
  setLoadingTrue,
  setLoadTrue,
  setLoadFail,
  getObjectFromArrayFor
} from 'utils/communication';

const defaultState = {
  isLoading: false,
  isLoad: false,
  isErrorLoad: false,
  error: { message: '', name: '' }
};

const stateCommunicationDefault = {
  cards: defaultState,
  reviews: {}
};

const communication = (
  stateCommunication = stateCommunicationDefault,
  { type, payload }
) => {
  switch (type) {
    case LOADING_CARDS:
      return {
        ...stateCommunication,
        cards: setLoadingTrue(stateCommunication.cards)
      };

    case LOAD_CARDS:
      return {
        ...stateCommunication,
        cards: setLoadTrue(stateCommunication.cards),
        reviews: getObjectFromArrayFor(payload.data, defaultState)
      };

    case LOAD_CARDS_FAIL:
      return {
        ...stateCommunication,
        cards: setLoadFail(stateCommunication.cards, payload)
      };

    case LOADING_REVIEWS:
      return {
        ...stateCommunication,
        reviews: {
          ...stateCommunication.reviews,
          [payload.id]: setLoadingTrue(stateCommunication.reviews[payload.id])
        }
      };

    case LOAD_REVIEWS:
      return {
        ...stateCommunication,
        reviews: {
          ...stateCommunication.reviews,
          [payload.id]: setLoadTrue(stateCommunication.reviews[payload.id])
        }
      };

    case LOAD_REVIEWS_FAIL:
      return {
        ...stateCommunication,
        reviews: {
          ...stateCommunication.reviews,
          [payload.id]: setLoadFail(
            stateCommunication.reviews[payload.id],
            payload
          )
        }
      };
    default:
      return stateCommunication;
  }
};

export default communication;
