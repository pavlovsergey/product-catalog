import {
  CREATE_USER,
  CREATE_USER_FAIL,
  LOGIN_USER,
  LOGIN_USER_FAIL,
  LOGOUT_USER
} from 'store/constants/auth';

export const stateAuthDefault = {
  role: '',
  username: '',
  errCreateUser: '',
  errLogInUser: ''
};

const auth = (stateAuth = stateAuthDefault, { type, payload }) => {
  switch (type) {
    case CREATE_USER:
      return {
        ...stateAuth,
        role: payload.role,
        username: payload.username,
        errCreateUser: ''
      };

    case CREATE_USER_FAIL:
      return { ...stateAuth, errCreateUser: payload.message };

    case LOGIN_USER:
      return {
        ...stateAuth,
        role: payload.role,
        username: payload.username,
        errCreateUser: ''
      };

    case LOGIN_USER_FAIL:
      return { ...stateAuth, errLogInUser: payload.message };

    case LOGOUT_USER:
      return stateAuthDefault;

    default:
      return stateAuth;
  }
};

export default auth;
