import {
  CREATE_NOTIFICATION,
  CLEAR_NOTIFICATION
} from 'store/constants/notification';

const stateDefault = {};

const notification = (stateNotification = stateDefault, { type, payload }) => {
  switch (type) {
    case CREATE_NOTIFICATION:
      return {
        ...stateNotification,
        [payload.id]: {
          id: payload.id,
          message: payload.message
        }
      };

    case CLEAR_NOTIFICATION:
      delete stateNotification[payload.id];
      return { ...stateNotification };

    default:
      return stateNotification;
  }
};

export default notification;
