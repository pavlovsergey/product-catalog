import { combineReducers } from 'redux';
import cards from './cards';
import reviews from './reviews';
import communication from './communication';
import auth from './auth';
import notification from './notification';

export default combineReducers({
  cards,
  reviews,
  communication,
  auth,
  notification
});
