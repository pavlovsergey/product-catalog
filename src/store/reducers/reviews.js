import { LOAD_REVIEWS, CREATE_REVIEW } from 'store/constants/reviews';

const stateReviewsDefault = {};

const reviews = (stateReviews = stateReviewsDefault, { type, payload }) => {
  switch (type) {
    case LOAD_REVIEWS:
      return {
        ...stateReviews,
        [payload.id]: payload.data
      };

    case CREATE_REVIEW:
      if (JSON.stringify(stateReviews) === '{}') return stateReviews;
      return {
        ...stateReviews,
        [payload.product]: [...stateReviews[payload.product], payload]
      };

    default:
      return stateReviews;
  }
};

export default reviews;
