import { LOAD_CARDS } from 'store/constants/cards';
import { getObjectFromArray } from 'utils/cards';

const stateCardDefault = {};

const cards = (stateCard = stateCardDefault, { type, payload }) => {
  switch (type) {
    case LOAD_CARDS:
      return getObjectFromArray(payload.data);

    default:
      return stateCard;
  }
};

export default cards;
