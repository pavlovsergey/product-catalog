import { LOAD_CARDS, MESSAGE_ERROR } from 'store/constants/cards';
import { LOADING_CARDS, LOAD_CARDS_FAIL } from 'store/constants/communication';
import { handlerError } from './error';
import { DOMAIN_PATH } from 'config';

import axios from 'axios';

axios.defaults.headers.common['Content-Type'] = 'application/json';

export const loadCards = () => {
  return async dispatch => {
    try {
      dispatch({ type: LOADING_CARDS });

      const { data } = await axios.get(`${DOMAIN_PATH}/api/products/`);
      return dispatch({
        type: LOAD_CARDS,
        payload: { data }
      });
    } catch (e) {
      const message = MESSAGE_ERROR;
      dispatch(handlerError(LOAD_CARDS_FAIL, message, e));
    }
  };
};
