import {
  LOAD_REVIEWS,
  MESSAGE_ERROR_LOAD_REVIEWS,
  CREATE_REVIEW,
  MESSAGE_ERROR_CREATE_REVIEW
} from 'store/constants/reviews';

import { CREATE_NOTIFICATION } from 'store/constants/notification';
import {
  LOADING_REVIEWS,
  LOAD_REVIEWS_FAIL
} from 'store/constants/communication';

import { handlerError } from './error';
import { DOMAIN_PATH } from 'config';

import axios from 'axios';

axios.defaults.headers.common['Content-Type'] = 'application/json';

export const loadReviews = id => {
  return async dispatch => {
    try {
      dispatch({ type: LOADING_REVIEWS, payload: { id } });

      const { data } = await axios.get(`${DOMAIN_PATH}/api/reviews/${id}`);
      return dispatch({
        type: LOAD_REVIEWS,
        payload: { data, id }
      });
    } catch (e) {
      const message = MESSAGE_ERROR_LOAD_REVIEWS;
      dispatch(handlerError(LOAD_REVIEWS_FAIL, message, e, { id }));
    }
  };
};

export const createReview = (rate, text, id) => {
  return async (dispatch, getState) => {
    const {
      auth: { username }
    } = getState();

    try {
      const { data } = await axios.post(`${DOMAIN_PATH}/api/reviews/${id}`, {
        rate,
        text
      });

      if (!data.success) {
        return dispatch(
          handlerError(CREATE_NOTIFICATION, MESSAGE_ERROR_CREATE_REVIEW)
        );
      }

      return dispatch({
        type: CREATE_REVIEW,
        payload: {
          id: Date.now(),
          product: id,
          rate,
          text,
          created_by: {
            username
          },
          created_at: new Date()
        }
      });
    } catch (e) {
      dispatch(
        handlerError(CREATE_NOTIFICATION, MESSAGE_ERROR_CREATE_REVIEW, e, {
          id: Date.now()
        })
      );
    }
  };
};
