import {
  CREATE_USER,
  CREATE_USER_FAIL,
  LOGIN_USER,
  LOGIN_USER_FAIL,
  LOGOUT_USER,
  ROLE
} from 'store/constants/auth';
import history from 'routes/history';
import { handlerError } from './error';
import { DOMAIN_PATH } from 'config';

import axios from 'axios';

axios.defaults.headers.common['Content-Type'] = 'application/json';

export const createUser = (username, password) => {
  return async dispatch => {
    try {
      const { data } = await axios.post(`${DOMAIN_PATH}/api/register/`, {
        username,
        password
      });

      if (!data.success) {
        return dispatch(handlerError(CREATE_USER_FAIL, data.message));
      }

      window.localStorage.setItem('token', data.token);
      return dispatch({
        type: CREATE_USER,
        payload: { role: ROLE.user, username }
      });
    } catch (e) {
      dispatch(handlerError(CREATE_USER_FAIL, '', e));
    }
  };
};

export const logInUser = (username, password) => {
  return async dispatch => {
    try {
      const { data } = await axios.post(`${DOMAIN_PATH}/api/login/`, {
        username,
        password
      });

      if (!data.success) {
        return dispatch(handlerError(LOGIN_USER_FAIL, data.message));
      }

      axios.defaults.headers.common['Authorization'] = `Token ${data.token}`;

      window.localStorage.setItem('token', data.token);
      return dispatch({
        type: LOGIN_USER,
        payload: { role: ROLE.user, username }
      });
    } catch (e) {
      dispatch(handlerError(LOGIN_USER_FAIL, '', e));
    }
  };
};

export const logOut = () => {
  history.push('/signIn');
  window.localStorage.removeItem('token');
  return { type: LOGOUT_USER };
};
