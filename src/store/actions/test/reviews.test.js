import configureMockStore from 'redux-mock-store';
import * as actions from '../reviews';
import * as typesReviews from '../../constants/reviews';
import * as types from '../../constants/communication';
import { DOMAIN_PATH } from 'config';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const mock = new MockAdapter(axios);
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions Reviews', () => {
  afterEach(() => {
    mock.reset();
  });

  it('SUCCESS - get data about reviews (action loadReviews)', () => {
    const id = 1;
    const data = [{ id }];
    mock.onGet(`${DOMAIN_PATH}/api/reviews/${id}`).reply(200, data);

    const expectedActions = [
      { type: types.LOADING_REVIEWS, payload: { id } },
      { type: typesReviews.LOAD_REVIEWS, payload: { data, id } }
    ];
    const store = mockStore({ reviews: {} });

    return store.dispatch(actions.loadReviews(id)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('FAIL - get data about reviews(action loadReviews)', () => {
    const id = 1;
    mock.onGet(`${DOMAIN_PATH}/api/reviews/${id}`).networkError();

    const expectedActions = [
      { type: types.LOADING_REVIEWS, payload: { id } },
      {
        type: types.LOAD_REVIEWS_FAIL,
        payload: {
          message: `${typesReviews.MESSAGE_ERROR_LOAD_REVIEWS} Network Error`,
          id
        }
      }
    ];

    const store = mockStore({ reviews: {} });
    return store.dispatch(actions.loadReviews(id)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('SUCCESS -  create Review (action  createReview)', () => {
    const id = 1,
      rate = 5,
      text = 'text',
      data = { success: true },
      username = 'username';

    mock.onPost(`${DOMAIN_PATH}/api/reviews/${id}`).reply(200, data);

    const realDate = global.Date;
    const realDateNow = global.Date.now;

    const date = jest.fn(() => 0);
    const dateNowStub = jest.fn(() => 0);

    global.Date = date;
    global.Date.now = dateNowStub;

    const expectedActions = [
      {
        type: typesReviews.CREATE_REVIEW,
        payload: {
          id: 0,
          product: id,
          rate,
          text,
          created_by: {
            username
          },
          created_at: {}
        }
      }
    ];

    const store = mockStore({ auth: { username } });
    return store.dispatch(actions.createReview(rate, text, id)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
      global.Date = realDate;
      global.Date.now = realDateNow;
    });
  });
});
