import configureMockStore from 'redux-mock-store';
import * as actions from '../cards';
import * as typesCards from '../../constants/cards';
import * as types from '../../constants/communication';
import { DOMAIN_PATH } from 'config';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const mock = new MockAdapter(axios);
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
  afterEach(() => {
    mock.reset();
  });

  it('SUCCESS - get data about cards(action loadCards)', () => {
    const data = [{ id: 1 }];
    mock.onGet(`${DOMAIN_PATH}/api/products/`).reply(200, data);

    const expectedActions = [
      { type: types.LOADING_CARDS },
      { type: typesCards.LOAD_CARDS, payload: { data } }
    ];
    const store = mockStore({ cards: {} });

    return store.dispatch(actions.loadCards()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('FAIL - get data about cards(action loadCards)', () => {
    mock.onGet(`${DOMAIN_PATH}/api/products/`).networkError();

    const expectedActions = [
      { type: types.LOADING_CARDS },
      {
        type: types.LOAD_CARDS_FAIL,
        payload: {
          message: `${typesCards.MESSAGE_ERROR} Network Error`
        }
      }
    ];

    const store = mockStore({ cards: {} });
    return store.dispatch(actions.loadCards()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
