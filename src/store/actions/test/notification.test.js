import { CLEAR_NOTIFICATION } from 'store/constants/notification';
import * as actions from '../notification';

describe('actions( clearNotificatioin )', () => {
  it('should create an action to clearNotificatioin', () => {
    const id = 1;
    const expectedAction = {
      type: CLEAR_NOTIFICATION,
      payload: {
        id
      }
    };
    expect(actions.clearNotificatioin(id)).toEqual(expectedAction);
  });
});
