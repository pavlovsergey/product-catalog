import configureMockStore from 'redux-mock-store';
import * as actions from '../auth';
import * as types from '../../constants/auth';
import { DOMAIN_PATH } from 'config';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const mock = new MockAdapter(axios);
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions auth', () => {
  afterEach(() => {
    mock.reset();
  });

  it('SUCCESS - create User (action createUser)', () => {
    const username = 'username';
    const data = { token: 1, success: true };
    mock.onPost(`${DOMAIN_PATH}/api/register/`).reply(200, data);

    const expectedActions = [
      { type: types.CREATE_USER, payload: { role: types.ROLE.user, username } }
    ];
    const store = mockStore({ auth: {} });

    return store.dispatch(actions.createUser(username)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('FAIL - create User (action createUser)', () => {
    mock.onPost(`${DOMAIN_PATH}/api/register/`).networkError();

    const expectedActions = [
      {
        type: types.CREATE_USER_FAIL,
        payload: {
          message: ` Network Error`
        }
      }
    ];

    const store = mockStore({ auth: {} });
    return store.dispatch(actions.createUser()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('SUCCESS - logIn User (action logInUser)', () => {
    const username = 'username';
    const data = { token: 1, success: true };
    mock.onPost(`${DOMAIN_PATH}/api/login/`).reply(200, data);

    const expectedActions = [
      { type: types.LOGIN_USER, payload: { role: types.ROLE.user, username } }
    ];
    const store = mockStore({ auth: {} });

    return store.dispatch(actions.logInUser(username)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('FAIL - logIn User (action logInUser)', () => {
    mock.onPost(`${DOMAIN_PATH}/api/login/`).networkError();

    const expectedActions = [
      {
        type: types.LOGIN_USER_FAIL,
        payload: {
          message: ` Network Error`
        }
      }
    ];

    const store = mockStore({ auth: {} });
    return store.dispatch(actions.logInUser()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should create an action to logOut', () => {
    const expectedAction = { type: types.LOGOUT_USER };
    expect(actions.logOut()).toEqual(expectedAction);
  });
});
