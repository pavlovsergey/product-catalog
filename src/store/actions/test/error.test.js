import * as actions from '../error';

describe('actions(error)', () => {
  const type = 'type';
  it('should create an action to error', () => {
    const expectedAction = {
      type,
      payload: {
        message: `message error`
      }
    };
    expect(actions.handlerError(type, 'message', { message: 'error' })).toEqual(
      expectedAction
    );
  });
});
