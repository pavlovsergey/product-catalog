import { CLEAR_NOTIFICATION } from 'store/constants/notification';

export const clearNotificatioin = id => {
  return { type: CLEAR_NOTIFICATION, payload: { id } };
};
