export const CREATE_USER = 'CREATE_USER';
export const CREATE_USER_FAIL = 'CREATE_USER_FAIL';
export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_USER_FAIL = 'LOGIN_USER_FAIL';
export const LOGOUT_USER = 'LOGOUT_USER';

export const ROLE = {
  user: 'USER'
};
