export const LOAD_REVIEWS = 'LOAD_REVIEWS';
export const CREATE_REVIEW = 'CREATE_REVIEW';

export const MESSAGE_ERROR_LOAD_REVIEWS =
  'Sorry, there was an error loading reviews';

export const MESSAGE_ERROR_CREATE_REVIEW = 'Sorry, the review was not created';
