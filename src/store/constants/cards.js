export const LOAD_CARDS = 'LOAD_CARDS';

export const MESSAGE_ERROR = 'Sorry, there was an error loading cards';
