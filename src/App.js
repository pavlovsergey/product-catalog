import React from 'react';
import Layout from './pages/Layout';
import { Provider } from 'react-redux';
import store from './store';
import { LastLocationProvider } from 'react-router-last-location';
import { Router } from 'react-router-dom';
import history from './routes/history';

const App = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <LastLocationProvider>
          <Layout />
        </LastLocationProvider>
      </Router>
    </Provider>
  );
};

export default App;
