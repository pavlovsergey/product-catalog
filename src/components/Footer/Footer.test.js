import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Footer from '.';

describe('Snapshot for component Footer', () => {
  it('should render correctly', () => {
    const component = shallow(<Footer />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
