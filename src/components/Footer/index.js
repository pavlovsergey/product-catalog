import React from 'react';
import styles from './Footer.module.scss';

const Footer = () => (
  <div className={styles.container}> &copy; Pavlov Sergey</div>
);

export default Footer;
