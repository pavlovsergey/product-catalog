import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import { clearNotificatioin } from 'store/actions/notification';
import { connect } from 'react-redux';

class Snack extends Component {
  state = {
    open: true
  };

  static propTypes = {
    message: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    clearNotificatioin: PropTypes.func.isRequired
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  clear = () => {
    const { id, clearNotificatioin } = this.props;
    clearNotificatioin(id);
  };
  componentDidMount() {
    setTimeout(this.clear, 5000);
  }

  render() {
    const { open } = this.state;
    const { message } = this.props;

    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        open={open}
        onClose={this.handleClose}
        autoHideDuration={4000}
        message={<span>{message} dfsfdsf</span>}
      />
    );
  }
}
const mapDispatchToProps = { clearNotificatioin };
export default connect(
  null,
  mapDispatchToProps
)(Snack);
