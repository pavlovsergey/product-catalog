import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Snack from '.';

describe('Snapshot for component Snack', () => {
  it('should render correctly component Snack', () => {
    const component = shallow(
      <Snack message="message" id={1} clearNotificatioin={() => {}} />
    );
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
