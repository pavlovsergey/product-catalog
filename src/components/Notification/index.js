import React from 'react';
import PropTypes from 'prop-types';
import Snack from './Snack';
import { connect } from 'react-redux';

const Notification = ({ notifications }) => (
  <div>
    {Object.values(notifications).map(notification => {
      return (
        <Snack
          key={notification.id}
          message={notification.message}
          id={notification.id}
        />
      );
    })}
  </div>
);

Notification.propTypes = {
  notifications: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return { notifications: state.notification };
};

export default connect(mapStateToProps)(Notification);
