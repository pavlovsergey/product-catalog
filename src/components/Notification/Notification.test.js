import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Notification from '.';

describe('Snapshot for component Notification', () => {
  it('should render correctly component Notification', () => {
    const component = shallow(<Notification notifications={{}} />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
