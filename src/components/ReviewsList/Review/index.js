import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Chip from '@material-ui/core/Chip';
import Rating from 'react-star-ratings';
import styles from './Review.module.scss';

class Review extends React.Component {
  render() {
    const {
      data: { created_by, created_at, text, rate }
    } = this.props;
    const date = new Date(Date.parse(created_at));
    const options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      weekday: 'long',
      hour: 'numeric',
      minute: 'numeric'
    };
    return (
      <Card className={styles.container}>
        <CardHeader
          className={styles.header}
          avatar={<Chip className={styles.name} label={created_by.username} />}
          action={
            <div className={styles.rating}>
              <Rating
                rating={rate}
                starDimension="22px"
                starRatedColor="#ffa726"
                numberOfStars={5}
                name="rating"
              />
            </div>
          }
          title={created_by.email}
          subheader={date.toLocaleString('en-US', options)}
        />

        <CardContent>{text}</CardContent>
      </Card>
    );
  }
}

Review.propTypes = {
  data: PropTypes.object.isRequired
};

export default Review;
