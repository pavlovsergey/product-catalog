import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import ReviewsList from '.';
import Review from './Review';

describe('Snapshot for components ReviewsList', () => {
  it('should render correctly component ReviewsList', () => {
    const component = shallow(
      <ReviewsList
        comunicationReviews={{}}
        reviews={[]}
        loadReviews={() => {}}
      />
    );
    expect(shallowToJson(component)).toMatchSnapshot();
  });

  it('should render correctly component Review', () => {
    const component = shallow(
      <Review
        data={{ created_by: {}, created_at: {}, text: 'text', rate: 3 }}
      />
    );
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
