import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Rating from 'react-star-ratings';
import Loader from 'components/Loader';
import Review from './Review';
import { connect } from 'react-redux';
import { loadReviews } from 'store/actions/reviews';
import styles from './ReviewsList.module.scss';
class ReviewsList extends Component {
  static propTypes = {
    comunicationReviews: PropTypes.object.isRequired,
    loadReviews: PropTypes.func.isRequired,
    reviews: PropTypes.array.isRequired
  };

  getRating = () => {
    const { reviews } = this.props;
    if (reviews.length === 0) return 0;
    let sum = 0;
    reviews.forEach(review => {
      sum += review.rate;
    });
    return sum / reviews.length;
  };
  componentDidMount() {
    const {
      comunicationReviews: { isLoading, isLoad, isErrorLoad },
      loadReviews,
      id
    } = this.props;

    if (!isLoading && !isLoad && !isErrorLoad) loadReviews(id);
  }
  render() {
    const { comunicationReviews, reviews } = this.props;
    return (
      <Loader comunication={[comunicationReviews]}>
        <div>
          <div className={styles.rating}>
            <Rating
              rating={this.getRating()}
              starRatedColor="#ffa726"
              starDimension="40px"
              numberOfStars={5}
              name="rating"
            />
          </div>
          <div>
            {reviews.map(review => (
              <Review key={review.id} data={review} />
            ))}
          </div>
        </div>
      </Loader>
    );
  }
}

const mapStateToProps = (state, { id }) => {
  return {
    comunicationReviews: state.communication.reviews[id],
    reviews: state.reviews[id] || []
  };
};
const mapDispatchToProps = { loadReviews };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReviewsList);
