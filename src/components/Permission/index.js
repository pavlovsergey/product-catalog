import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Permission extends Component {
  static propTypes = {
    roles: PropTypes.array.isRequired,
    authRole: PropTypes.string.isRequired,
    componentDefault: PropTypes.node,
    children: PropTypes.node.isRequired,
    privateProps: PropTypes.object
  };

  render() {
    const { privateProps } = this.props;
    const { roles, children, authRole, ComponentDefault } = this.props;

    if (roles.indexOf(authRole) !== -1) return children;
    return <ComponentDefault {...privateProps} /> || null;
  }
}

const mapStateToProps = state => {
  return {
    authRole: state.auth.role
  };
};

export default connect(mapStateToProps)(Permission);
