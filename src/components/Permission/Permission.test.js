import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Permission from '.';

describe('Snapshot for component Permission', () => {
  it('should render correctly component Permission', () => {
    const component = shallow(
      <Permission
        roles={[]}
        authRole=""
        privateProps={{}}
        componentDefault={<div />}
        children={<div />}
      />
    );
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
