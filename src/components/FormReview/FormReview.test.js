import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import FormReview from '.';

describe('Snapshot for component FormReview', () => {
  it('should render correctly component FormReview', () => {
    const component = shallow(<FormReview />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
