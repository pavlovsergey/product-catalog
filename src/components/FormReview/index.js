import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Rating from 'react-star-ratings';
import { createReview } from 'store/actions/reviews';
import { connect } from 'react-redux';
import styles from './FormReview.module.scss';

class FormReview extends Component {
  state = {
    rate: 0,
    text: '',
    isSend: false,
    isError: false
  };

  static propTypes = {
    classes: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    createReview: PropTypes.func.isRequired
  };

  setRate = rate => {
    this.setState({ rate });
  };

  handleChangeText = ({ target: { value } }) => {
    this.setState({ text: value });
  };

  send = () => {
    const { rate, text } = this.state;
    const { id, createReview } = this.props;
    createReview(rate, text, id);
  };

  handleSendReview = () => {
    const { text } = this.state;

    if (text) {
      return this.setState({ isSend: true }, this.send);
    }

    return this.setState({ isError: true });
  };

  render() {
    const { rate, isSend, isError } = this.state;
    const { classes } = this.props;

    if (isSend) {
      return (
        <div className={styles.container}>
          <Paper className={styles.body} elevation={1}>
            <h3 className={styles.send}>Thank You!</h3>
          </Paper>
        </div>
      );
    }
    return (
      <div className={styles.container}>
        <Paper className={styles.body} elevation={1}>
          <h3 className={styles.rate}>
            {`Your rate : `}
            <Rating
              rating={rate}
              starRatedColor="#ffa726"
              starDimension="40px"
              numberOfStars={5}
              changeRating={this.setRate}
            />
          </h3>

          <TextField
            error={isError}
            label=" Write your review"
            variant="outlined"
            multiline
            fullWidth
            rows={5}
            onChange={this.handleChangeText}
            InputProps={{
              classes: {
                root: classes.text
              }
            }}
            InputLabelProps={{
              classes: {
                root: classes.text
              }
            }}
          />
          <div className={styles.error}>
            {isError && 'Please enter a review'}
          </div>
          <Button
            variant="contained"
            onClick={this.handleSendReview}
            color="primary"
            className={styles.btn}
          >
            Send
            <SendIcon className={styles.icon} />
          </Button>
        </Paper>
      </div>
    );
  }
}

const stylesTextField = theme => ({
  text: {
    fontSize: 26,
    textAlign: 'center'
  }
});

const mapDispatchToProps = { createReview };
export default withStyles(stylesTextField)(
  connect(
    null,
    mapDispatchToProps
  )(FormReview)
);
