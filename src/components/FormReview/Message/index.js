import React from 'react';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import styles from './Message.module.scss';

const Message = () => {
  return (
    <div className={styles.container}>
      <Paper className={styles.body} elevation={1}>
        <h3>You are not registred</h3>

        <p>
          <Link to={'/signIn'}>Login</Link> or{' '}
          <Link to={'/signUp'}>register</Link> to leave a review.
        </p>
      </Paper>
    </div>
  );
};

export default Message;
