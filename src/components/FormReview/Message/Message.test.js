import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Message from '.';

describe('Snapshot for component Message', () => {
  it('should render correctly component Message', () => {
    const component = shallow(<Message />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
