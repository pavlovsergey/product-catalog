import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Person from '@material-ui/icons/Person';
import SendIcon from '@material-ui/icons/Send';
import { Link } from 'react-router-dom';
import { validationPasword, validationUserName } from 'utils/validation';
import { withLastLocation } from 'react-router-last-location';
import history from 'routes/history';
import { logInUser } from 'store/actions/auth';
import { connect } from 'react-redux';
import styles from './LogIn.module.scss';

const err = { isError: false, userName: '', password: '', repeatPassword: '' };

class LogIn extends Component {
  static propTypes = {
    logInUser: PropTypes.func.isRequired,
    role: PropTypes.string.isRequired
  };
  state = {
    userName: '',
    password: '',
    showPassword: false,
    repeatPassword: '',
    showRepeatPassword: false,
    errors: err
  };

  handleEnterPress = e => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      this.handleSubmit();
    }
  };

  handleSubmit = () => {
    const { logInUser, err } = this.props;
    const { userName, password } = this.state;
    let errors = { ...err };

    validationUserName(userName, errors);
    validationPasword(password, errors);

    if (errors.isError) return this.setState({ errors });
    logInUser(userName, password);
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  checkAuthorization = () => {
    const { lastLocation, role } = this.props;

    if (role && lastLocation && lastLocation.pathname !== '/signUp') {
      return history.goBack();
    }

    if (role) history.push('/');
  };

  componentDidMount() {
    this.checkAuthorization();
  }

  componentDidUpdate() {
    this.checkAuthorization();
  }

  render() {
    const { userName, password, showPassword, errors } = this.state;
    const { err } = this.props;

    return (
      <div onKeyDown={this.handleEnterPress}>
        <h3 className={styles.label}>SignIn</h3>

        <div className={styles.error}>{err && `User not logIn. ${err}`}</div>

        <div className={styles.field}>
          <TextField
            fullWidth
            variant="outlined"
            type={'text'}
            label="User name"
            value={userName}
            onChange={this.handleChange('userName')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Person />
                </InputAdornment>
              )
            }}
          />
        </div>

        <div className={styles.error}>{errors.isError && errors.userName}</div>

        <div className={styles.field}>
          <TextField
            fullWidth
            variant="outlined"
            type={showPassword ? 'text' : 'password'}
            label="Password"
            value={password}
            onChange={this.handleChange('password')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.handleClickShowPassword}
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </div>

        <div className={styles.error}>{errors.isError && errors.password}</div>

        <Button
          fullWidth
          variant="contained"
          onClick={this.handleSubmit}
          color="primary"
          className={styles.btn}
        >
          logIn
          <SendIcon className={styles.icon} />
        </Button>
        <Link to={'/signUp'} className={styles.link}>
          go to Sign Up
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    role: state.auth.role,
    err: state.auth.errLogInUser
  };
};

const mapDispatchToProps = { logInUser };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withLastLocation(LogIn));
