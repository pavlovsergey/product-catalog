import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Registration from '.';

describe('Snapshot for component Registration', () => {
  it('should render correctly component Registration', () => {
    const component = shallow(<Registration role="" logInUser={() => {}} />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
