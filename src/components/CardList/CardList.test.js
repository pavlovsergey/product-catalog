import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import CardList from '.';
import ProductCard from './ProductCard';
import CardImage from './ProductCard/CardImage';
describe('Snapshot for components CardList', () => {
  it('should render correctly component CardList', () => {
    const component = shallow(<CardList cards={{}} />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });

  it('should render correctly component ProductCard', () => {
    const component = shallow(<ProductCard card={{ title: '#', img: '#' }} />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });

  it('should render correctly component CardImage', () => {
    const component = shallow(<CardImage />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
