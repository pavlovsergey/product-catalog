import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardImage from './CardImage';
import history from 'routes/history';
import styles from './ProductCard.module.scss';

class ProductCard extends Component {
  static propTypes = {
    card: PropTypes.object.isRequired
  };

  handleRedirect = () => history.push(`/product/${this.props.card.id}`);

  render() {
    const {
      card: { title, img }
    } = this.props;

    return (
      <Card className={styles.card}>
        <CardActionArea onClick={this.handleRedirect}>
          <CardImage image={img} />
        </CardActionArea>

        <div className={styles.title}>{title}</div>
      </Card>
    );
  }
}

export default ProductCard;
