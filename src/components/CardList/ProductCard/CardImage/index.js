import React from 'react';
import PropTypes from 'prop-types';
import LoaderImage from 'components/LoaderImage';
import styles from './CardImage.module.scss';
import { DOMAIN_PATH } from 'config';

const CardImage = ({ image }) => {
  return (
    <div className={styles.container}>
      <LoaderImage
        src={`${DOMAIN_PATH}/static/${image}`}
        defaultSrc="/defaultProduct.jpg"
      >
        {src => <img className={styles.img} src={src} alt="card" />}
      </LoaderImage>
    </div>
  );
};
CardImage.propTypes = {
  images: PropTypes.string
};

export default CardImage;
