import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import LoaderImage from '.';

describe('Snapshot for component LoaderImage', () => {
  it('should render correctly component LoaderImage', () => {
    const component = shallow(<LoaderImage children={() => {}} />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
