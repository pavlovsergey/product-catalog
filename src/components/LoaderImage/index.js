import React, { Component } from 'react';
import Spinner from 'components/Spinner';
import PropTypes from 'prop-types';
import styles from './LoaderImage.module.scss';

class LoaderImage extends Component {
  state = {
    isLoadImg: false,
    src: ''
  };
  static propTypes = {
    src: PropTypes.string,
    defaultSrc: PropTypes.string,
    children: PropTypes.func.isRequired
  };

  loadImage = () => {
    const { src } = this.props;
    this.setState({ isLoadImg: true, src }, this.removeListeners);
  };

  loadImageFail = () => {
    const { defaultSrc } = this.props;
    this.setState({ isLoadImg: true, src: defaultSrc }, this.removeListeners);
  };

  removeListeners = () => {
    this.img.removeEventListener('load', this.loadImage);
    this.img.removeEventListener('error', this.loadImageFail);
    delete this.img;
  };

  componentDidMount() {
    const { src } = this.props;
    this.img = new Image();

    if (!src) return this.loadImageFail();

    this.img.src = src;
    this.img.addEventListener('load', this.loadImage);
    this.img.addEventListener('error', this.loadImageFail);
  }

  componentWillUnmount() {
    if (this.img) this.removeListeners();
  }
  render() {
    const { children } = this.props;
    const { isLoadImg, src } = this.state;
    if (isLoadImg) return children(src);
    return (
      <div className={styles.container}>
        <Spinner />
      </div>
    );
  }
}

export default LoaderImage;
