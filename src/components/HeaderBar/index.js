import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconAccount from './IconAccount';
import styles from './HeaderBar.module.scss';

const HeaderBar = () => (
  <div className={styles.container}>
    <AppBar position="static">
      <Toolbar className={styles.body}>
        <h1 className={styles.brand}>
          <span className={styles.firstPart}>Product</span>
          <span className={styles.secondPart}>Catalog</span>
        </h1>
        <IconAccount />
      </Toolbar>
    </AppBar>
  </div>
);

export default HeaderBar;
