import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import HeaderBar from '.';
import IconAccount from './IconAccount';
import PrivateMenu from './IconAccount/PrivateMenu';

describe('Snapshot for components HeaderBar', () => {
  it('should render correctly component HeaderBar', () => {
    const component = shallow(<HeaderBar />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });

  it('should render correctly component IconAccount', () => {
    const component = shallow(<IconAccount />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });

  it('should render correctly component PrivateMenu', () => {
    const component = shallow(<PrivateMenu handleCloseMenu={() => {}} />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
