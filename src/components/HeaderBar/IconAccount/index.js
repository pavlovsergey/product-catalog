import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Permission from 'components/Permission';
import PrivateMenu from './PrivateMenu';
import { ROLE } from 'store/constants/auth';
import { Link } from 'react-router-dom';
import { logOut } from 'store/actions/auth';
import { connect } from 'react-redux';
import styles from './IconAccount.module.scss';

class IconAccount extends Component {
  state = {
    anchorEl: null
  };
  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  handleCloseMenu = () => {
    this.setState({ anchorEl: null });
  };

  handleLogOut = () => {
    this.setState({ anchorEl: null }, this.LogOut);
  };

  LogOut = () => {
    this.props.logOut();
  };

  render() {
    const { anchorEl } = this.state;
    const isOpenMenu = Boolean(anchorEl);
    return (
      <div>
        <IconButton
          aria-haspopup="true"
          onClick={this.handleMenu}
          color="inherit"
          aria-label="profile"
        >
          <AccountCircle />
        </IconButton>
        <Menu
          className={styles.menu}
          anchorEl={anchorEl}
          open={isOpenMenu}
          onClose={this.handleCloseMenu}
        >
          <Permission
            roles={[ROLE.user]}
            ComponentDefault={PrivateMenu}
            privateProps={{ handleCloseMenu: this.handleCloseMenu }}
          >
            <Link onClick={this.handleLogOut} className={styles.link} to={'/'}>
              <MenuItem>LogOut</MenuItem>
            </Link>
          </Permission>
        </Menu>
      </div>
    );
  }
}

const mapDispatchToProps = { logOut };

export default connect(
  null,
  mapDispatchToProps
)(IconAccount);
