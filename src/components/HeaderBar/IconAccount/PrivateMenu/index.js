import React from 'react';
import PropTypes from 'prop-types';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import styles from './PrivateMenu.module.scss';

const PrivateMenu = ({ handleCloseMenu }) => (
  <React.Fragment>
    <Link onClick={handleCloseMenu} className={styles.link} to={'/signIn'}>
      <MenuItem>Sign In</MenuItem>
    </Link>
    <Link onClick={handleCloseMenu} className={styles.link} to={'/signUp'}>
      <MenuItem>Sign Up</MenuItem>
    </Link>
  </React.Fragment>
);

PrivateMenu.propTypes = {
  handleCloseMenu: PropTypes.func.isRequired
};

export default PrivateMenu;
