import React from 'react';
import PropTypes from 'prop-types';
import styles from './Error.module.scss';

const Error = ({ error: { message } = {} }) => {
  const labelText = message ? `${message}` : 'Sorry an error has occurred.';

  return <div className={styles.container}>{labelText}</div>;
};

Error.propTypes = {
  error: PropTypes.object
};

export default Error;
