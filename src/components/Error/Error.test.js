import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Error from '.';

describe('Snapshot for component Error', () => {
  it('should render correctly componentError', () => {
    const component = shallow(<Error />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
