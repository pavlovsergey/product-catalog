import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Spinner from '.';

describe('Snapshot for component Spinner', () => {
  it('should render correctly component Spinner', () => {
    const component = shallow(<Spinner />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
