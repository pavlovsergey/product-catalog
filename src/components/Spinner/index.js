import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import styles from './Spinner.module.scss';

const Spinner = () => (
  <div className={styles.scssspinner}>
    <LinearProgress variant="query" />
  </div>
);

export default Spinner;
