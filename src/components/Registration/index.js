import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Person from '@material-ui/icons/Person';
import SendIcon from '@material-ui/icons/Send';
import { Link } from 'react-router-dom';
import {
  validationPasword,
  validationUserName,
  validationRepeatPassword
} from 'utils/validation';
import { withLastLocation } from 'react-router-last-location';
import history from 'routes/history';
import { createUser } from 'store/actions/auth';
import { connect } from 'react-redux';
import styles from './Registration.module.scss';

const err = { isError: false, userName: '', password: '', repeatPassword: '' };

class Registration extends Component {
  static propTypes = {
    createUser: PropTypes.func.isRequired,
    role: PropTypes.string.isRequired
  };
  state = {
    userName: '',
    password: '',
    showPassword: false,
    repeatPassword: '',
    showRepeatPassword: false,
    errors: err
  };

  handleEnterPress = e => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      this.handleCreateUser();
    }
  };

  handleCreateUser = () => {
    const { createUser, err } = this.props;
    const { userName, password, repeatPassword } = this.state;
    let errors = { ...err };

    validationUserName(userName, errors);
    validationPasword(password, errors);
    validationRepeatPassword(password, repeatPassword, errors);
    if (errors.isError) return this.setState({ errors });
    createUser(userName, password);
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleClickShowPassword = prop => () => {
    this.setState(state => ({ [prop]: !state[prop] }));
  };

  checkAuthorization = () => {
    const { lastLocation, role } = this.props;

    if (role && lastLocation && lastLocation.pathname !== '/signIn') {
      return history.goBack();
    }

    if (role) history.push('/');
  };

  componentDidMount() {
    this.checkAuthorization();
  }

  componentDidUpdate() {
    this.checkAuthorization();
  }
  render() {
    const {
      userName,
      password,
      showPassword,
      repeatPassword,
      showRepeatPassword,
      errors
    } = this.state;
    const { err } = this.props;
    return (
      <div onKeyDown={this.handleEnterPress}>
        <h3 className={styles.label}>SignUp</h3>

        <div className={styles.error}>{err}</div>

        <div className={styles.field}>
          <TextField
            fullWidth
            variant="outlined"
            type={'text'}
            label="User name"
            value={userName}
            onChange={this.handleChange('userName')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Person />
                </InputAdornment>
              )
            }}
          />
        </div>

        <div className={styles.error}>{errors.isError && errors.userName}</div>

        <div className={styles.field}>
          <TextField
            fullWidth
            variant="outlined"
            type={showPassword ? 'text' : 'password'}
            label="Password"
            value={password}
            onChange={this.handleChange('password')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.handleClickShowPassword('showPassword')}
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </div>

        <div className={styles.error}>{errors.isError && errors.password}</div>

        <div className={styles.field}>
          <TextField
            fullWidth
            variant="outlined"
            type={showRepeatPassword ? 'text' : 'password'}
            label="Rpeat password"
            value={repeatPassword}
            onChange={this.handleChange('repeatPassword')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.handleClickShowPassword('showRepeatPassword')}
                  >
                    {showRepeatPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </div>
        <div className={styles.error}>
          {errors.isError && errors.repeatPassword}
        </div>
        <Button
          fullWidth
          variant="contained"
          onClick={this.handleCreateUser}
          color="primary"
          className={styles.btn}
        >
          Join now
          <SendIcon className={styles.icon} />
        </Button>
        <Link to={'/signIn'} className={styles.link}>
          go to Sign In
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    role: state.auth.role,
    err: state.auth.errCreateUser
  };
};

const mapDispatchToProps = { createUser };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withLastLocation(Registration));
