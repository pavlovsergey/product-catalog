import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Error from 'components/Error';
import Spinner from 'components/Spinner';
import styles from './Loader.module.scss';

class Loader extends Component {
  static propTypes = {
    comunication: PropTypes.array.isRequired,
    children: PropTypes.node.isRequired
  };
  getError = () => {
    const { comunication } = this.props;
    let error;
    const isErrorLoad = comunication.some(err => {
      if (err.isErrorLoad) {
        error = err.error;
        return true;
      }
      return false;
    });

    return { isErrorLoad, error };
  };

  getLoading = () => {
    const { comunication } = this.props;
    return comunication.some(loading => {
      return loading.isLoading;
    });
  };

  getLoad = () => {
    const { comunication } = this.props;
    return comunication.some(load => {
      return load.isLoad;
    });
  };

  render() {
    const err = this.getError();
    const { children } = this.props;
    if (err.isErrorLoad) return <Error error={err.error} />;

    if (this.getLoading()) {
      return (
        <div className={styles.spinner}>
          <Spinner />
        </div>
      );
    }

    if (this.getLoad()) return children;
    return null;
  }
}
export default Loader;
