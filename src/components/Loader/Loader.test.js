import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Loader from '.';

describe('Snapshot for component Loader', () => {
  it('should render correctly component Loader', () => {
    const component = shallow(<Loader comunication={[]} children={<div />} />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
