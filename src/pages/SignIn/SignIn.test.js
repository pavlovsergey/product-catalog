import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import SignIn from '.';

describe('Snapshot for component SignIn', () => {
  it('should render correctly component SignIn', () => {
    const component = shallow(<SignIn />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
