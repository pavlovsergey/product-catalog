import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import LogIn from 'components/LogIn';
import styles from './SignIn.module.scss';

const SignIn = () => (
  <div className={styles.container}>
    <div className={styles.body}>
      <Link to={'/'} className={styles.link}>
        <Button className={styles.btn}>
          go to
          <div className={styles.brand}>
            <span className={styles.firstPart}>Product</span>
            <span className={styles.secondPart}>Catalog</span>
          </div>
        </Button>
      </Link>
      <div className={styles.content}>
        <LogIn />
      </div>
    </div>
  </div>
);

export default SignIn;
