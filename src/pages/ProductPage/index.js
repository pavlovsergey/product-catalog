import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import KeyboardReturn from '@material-ui/icons/KeyboardReturn';
import SwipeableViews from 'react-swipeable-views';
import ReviewsList from 'components/ReviewsList';
import LoaderImage from 'components/LoaderImage';
import Message from 'components/FormReview/Message';
import FormReview from 'components/FormReview';
import Permission from 'components/Permission';
import { ROLE } from 'store/constants/auth';
import { DOMAIN_PATH } from 'config';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import styles from './ProductPage.module.scss';

class ProductPage extends Component {
  static propTypes = {
    card: PropTypes.object.isRequired
  };

  state = {
    valueTab: 0
  };

  handleChangeTabs = (event, value) => {
    this.setState({ valueTab: value });
  };

  render() {
    const {
      card: { title, img, text },
      id
    } = this.props;
    const { valueTab } = this.state;

    return (
      <div className={styles.container}>
        <Grid container direction="row" justify="center">
          <Grid
            item
            className={styles.body}
            xs={12}
            sm={12}
            md={11}
            lg={10}
            xl={9}
          >
            <Link to={'/'}>
              <Button className={styles.btn}>
                <KeyboardReturn />
                Back
              </Button>
            </Link>
            <h1 className={styles.header}>{title}</h1>
            <div className={styles.containerImg}>
              <LoaderImage
                src={`${DOMAIN_PATH}/static/${img}`}
                defaultSrc="/defaultProduct.jpg"
              >
                {src => <img className={styles.img} src={src} alt={title} />}
              </LoaderImage>
            </div>
            <Paper square>
              <Tabs
                className={styles.tabs}
                value={valueTab}
                onChange={this.handleChangeTabs}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="primary"
              >
                <Tab label="Description" />
                <Tab label="Reviews" />
                <Tab label="Write review" />
              </Tabs>
            </Paper>
            <div className={styles.content}>
              <SwipeableViews axis={'x-reverse'} index={valueTab}>
                {(valueTab === 0 && <p>{text}</p>) || <div />}
                {(valueTab === 1 && <ReviewsList id={id} />) || <div />}
                {(valueTab === 2 && (
                  <div>
                    <Permission roles={[ROLE.user]} ComponentDefault={Message}>
                      <FormReview id={id} />
                    </Permission>
                  </div>
                )) || <div />}
              </SwipeableViews>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state, { id }) => {
  return {
    card: state.cards[id]
  };
};

export default connect(mapStateToProps)(ProductPage);
