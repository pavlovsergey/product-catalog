import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import ProductPage from '.';

describe('Snapshot for component ProductPage', () => {
  it('should render correctly ProductPage', () => {
    const component = shallow(<ProductPage />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
