import React from 'react';
import PropTypes from 'prop-types';
import CardList from 'components/CardList';
import { connect } from 'react-redux';
import styles from './Home.module.scss';

const Home = ({ cards }) => (
  <div className={styles.container}>
    <CardList cards={cards} />
  </div>
);

Home.propTypes = {
  cards: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    cards: state.cards
  };
};

export default connect(mapStateToProps)(Home);
