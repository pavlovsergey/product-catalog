import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Registration from 'components/Registration';
import styles from './SignUp.module.scss';

const SignUp = () => (
  <div className={styles.container}>
    <div className={styles.body}>
      <Link to={'/'} className={styles.link}>
        <Button className={styles.btn}>
          go to
          <div className={styles.brand}>
            <span className={styles.firstPart}>Product</span>
            <span className={styles.secondPart}>Catalog</span>
          </div>
        </Button>
      </Link>
      <div className={styles.content}>
        <Registration />
      </div>
    </div>
  </div>
);

export default SignUp;
