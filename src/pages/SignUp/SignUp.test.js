import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import SignUp from '.';

describe('Snapshot for component SignUp', () => {
  it('should render correctly component SignUp', () => {
    const component = shallow(<SignUp />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
