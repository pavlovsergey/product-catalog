import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Layout from '.';

describe('Snapshot for component Layout', () => {
  it('should render correctly', () => {
    const component = shallow(<Layout />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
