import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Routes from 'routes';
import Loader from 'components/Loader';
import HeaderBar from 'components/HeaderBar';
import Notification from 'components/Notification';
import Footer from 'components/Footer';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { loadCards } from 'store/actions/cards';
import styles from './Layout.module.scss';

class Layout extends Component {
  static propTypes = {
    comunicationCards: PropTypes.object.isRequired,
    loadCards: PropTypes.func.isRequired
  };

  componentDidMount() {
    const {
      comunicationCards: { isLoading, isLoad, isErrorLoad },
      loadCards
    } = this.props;

    if (!isLoading && !isLoad && !isErrorLoad) loadCards();
  }
  render() {
    const { comunicationCards } = this.props;
    return (
      <div className={styles.container}>
        <HeaderBar />
        <Loader comunication={[comunicationCards]}>
          <div className={styles.wrapper}>
            <Routes />
          </div>
        </Loader>
        <Notification />
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    comunicationCards: state.communication.cards
  };
};
const mapDispatchToProps = { loadCards };

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Layout)
);
