import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import App from './App';

describe('Snapshot for component App', () => {
  it('should render correctly', () => {
    const component = shallow(<App />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
